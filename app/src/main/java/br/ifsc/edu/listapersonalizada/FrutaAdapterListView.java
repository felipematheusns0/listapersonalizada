package br.ifsc.edu.listapersonalizada;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.text.DecimalFormat;
import java.util.List;

class FrutaAdapterListView extends ArrayAdapter {
    Context mContext;
    int mLayout;

    public FrutaAdapterListView(@NonNull Context context, int resource, @NonNull List objects) {
        super(context, resource, objects);

        this.mContext = context;
        this.mLayout = resource;


    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        convertView = inflater.inflate(mLayout, null);

        TextView tvCodigo = convertView.findViewById(R.id.tvCodigo);
        TextView tvNome = convertView.findViewById(R.id.tvNome);
        TextView tvPreco = convertView.findViewById(R.id.tvPreco);
        TextView tvPrecoVenda = convertView.findViewById(R.id.tvPrecoVenda);
        ImageView imageView = convertView.findViewById(R.id.imageView);

        Fruta fruta = (Fruta) getItem(position);

        DecimalFormat d = new DecimalFormat("#,###,00");

        tvCodigo.setText(Integer.toString(fruta.getCodigo()));
        tvNome.setText(fruta.getNome());
        tvPreco.setText(d.format(fruta.getPreco()));
        tvPrecoVenda.setText(d.format(fruta.getPreco_venda()));
        imageView.setImageResource(fruta.getImagem());


        return convertView;
    }
}
