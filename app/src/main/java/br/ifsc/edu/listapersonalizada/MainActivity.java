package br.ifsc.edu.listapersonalizada;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void list(View view) {
        Intent intent = new Intent(MainActivity.this, ListagemListView.class);
        startActivity(intent);
    }

    public void recycler(View view) {
        Intent intent = new Intent(MainActivity.this, ListagemRecyclerView.class);
        startActivity(intent);
    }
}
