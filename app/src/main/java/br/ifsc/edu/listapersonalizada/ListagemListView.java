package br.ifsc.edu.listapersonalizada;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

public class ListagemListView extends AppCompatActivity {
    FrutasController frutasController;
    ListView listView;
    Fruta fruta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listagem_list_view);

        listView = findViewById(R.id.listview);
        frutasController = new FrutasController();
        fruta= new Fruta();

        ArrayList<Fruta> listaFrutas = new ArrayList<>();
        for(Fruta f:frutasController.FRUTAS){
            listaFrutas.add(f);
        }

        FrutaAdapterListView frutaAdapter = new FrutaAdapterListView(
                getApplicationContext(),
                R.layout.modelo_listagem_fruta,
                listaFrutas

        );

        listView.setAdapter(frutaAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getApplicationContext(),Exibe.class );

                intent.putExtra("id",i);

                startActivity(intent);
            }
        });


    }
}
